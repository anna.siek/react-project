import { fetchOffers } from './services';

const api = {
    fetchOffers
}

export default api;