import { useState} from 'react';

function ContactForm() {

    const [name, setName] = useState('');
    const [message, setmessage] = useState('');

    const handleChange = (event) => {
        // console.log();
        // const value = event.target.value;

    }
    const handleSubmit = (event) => {
        event.preventDefault();
        // const {name} = event.target;
        // const request = {
        //     name,
        //     message
        // };
    };

    return (
        <form className="ContactForm" onSubmit={handleSubmit}>
            <div className="ContactForm__row">
                <label>Name</label>
                <input name="name" onChange={handleChange}/>
            </div>
            <div className="ContactForm__row">
                <label>Message</label>
                <textarea name="message" onChange={handleChange}/>
            </div>
            <div className="ContactForm__row">
                <button className="button" type="submit">Send</button>
            </div>
        </form>
    );
}

export default ContactForm;