import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
const offerStyles = {
    main: {
        backgroundColor: '#10101e'
    },
    h2: {
        cursor: 'pointer',
        color: '#1f2527'
    }
};

function OfferListItem({ 
    id,
    cover = "https://cf.bstatic.com/xdata/images/hotel/square90/79172064.webp?k=9af8e5c75879a392d627595f8fad96d205e23ed264ec3744476b57805fa28731&o=", 
    name = "Oferta", 
    description = "Opis oferty", 
    country = "Polska",
    city,
    handleClick
  }) {
  return (
    <div className="offer" style={offerStyles.main} onClick={() => handleClick(name)}>
      <div className="offer__img">
        <img src={cover} alt={name} />
      </div>
      <div>
        <h2 
          style={offerStyles.h2}
        >
            <Link to={`/offer/${id}`}>
                {name}
            </Link>
            
        </h2>
        <p>{description}</p>
        <p>{city}, {country}</p>
      </div>

    </div>
  );
}

OfferListItem.propTypes = {
  name: PropTypes.string.isRequired, 
}

OfferListItem.defaultProps = {
  description: 'Brakuje opisu'
}

export default OfferListItem;