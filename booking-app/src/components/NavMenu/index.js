// import { Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

const activeStyles = {
    backgroundColor: 'rgb(16 16 30)',
    color: '#fff'
}

function NavMenu() {
    return (
        <nav>
            <ul>
                <li>
                    <NavLink activeStyle={activeStyles} to="/" exact>Home</NavLink>
                </li>
                <li>
                    <NavLink activeStyle={activeStyles} to="/contact">Contact</NavLink>
                </li>
            </ul>
        </nav>
    );
}


export default NavMenu;