export const fetchOffers = async () => {
    try {
        const response = await fetch('/data/offers.json'); 
        const data = await response.json();

        //modify you data as you want

        return data;
        
    } catch (error) {
        throw Error('Ooops');
    }
}

export const fetchOffer = async (id) => {
    try {
        const response = await fetch('/data/offers.json'); 
        const data = await response.json();
        const filteredResults =  data.filter((item)=> {
           return  parseInt(item.id, 10)===parseInt(id, 10);
        });
        if (filteredResults.length) {
            return filteredResults[0];
        } else {
            throw Error('Results not found');
        }
        
    } catch (error) {
        throw Error('Ooops');
    }
}