import { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { fetchOffer } from '../services';


function OfferPage() {
    const { offerId } = useParams();
    const [ offer, setOffer ] = useState(null);
    const history = useHistory();

    useEffect(() => {
        async function fetchData() {
            try {
                const data = await fetchOffer(offerId); 
                setOffer(data);
            } catch (error) {
                console.log(`UI Error ${error}`);
            }
        }
        fetchData();
    }, [offerId]);

    const handleClick = () => {
        if (offer) {
            history.push(`/offer/${parseInt(offer.id, 10 ) + 1}`);
        }
        
    };

    const Indicator = () => (<h1>Loading ...</h1>);

    const Content = ({offer, handleClick}) => {
        const {name, street, city, country } = offer;
        return (
            <>
                <h1>#{offerId} - {name}</h1>
                <p>{street}, {city}, {country}</p>
                <hr /> 
                <button className="button" onClick={handleClick}>Next offer</button>
            </>
        );
    };

    return (
        <div>
            { offer 
                ? <Content offer={offer} handleClick={handleClick}/> 
                : <Indicator />
            }
            
        </div>
    );
}

export default OfferPage;