import { useState, useEffect } from 'react';
import { fetchOffers } from '../services'; 
import OfferListItem from '../components/OfferListItem';


function Home() {

  const [offers, setOffers] = useState([]);

  const [selectedOffer, setSelectedOffer] = useState('');

  useEffect(() => {
    setSelectedOffer('No selection')
  }, []);


  useEffect(() => {
    async function fetchData() {
      try {
        const data = await fetchOffers();
        setOffers(data);
      } catch (error) {
        
      }
    }
    fetchData();
  }, []);

  return (
    <div className="Home">
      <div>
        <h1>Select offer</h1>
        <h2>{selectedOffer}</h2>
      </div>     
       {offers.map((elem) => 
          <OfferListItem 
            key={`offer-id${elem.id}`}
            // offerObj={elem}
            id={elem.id}
            handleClick={setSelectedOffer}
            description={elem.description}
            country={elem.country}
            cover={elem.cover}
            name={elem.name}
            city={elem.city}
          />
        )}
    </div>
  );
}

export default Home;
